﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveDuplicate
{
    public class RemoveDuplicateElement
    {
        public int DeleteElement(int[] finalArray, int size)
        {
            int[] temp = new int[size];
            int j = 0;

            for (int i = 0; i < size - 1; i++)
            {
                if (finalArray[i] != finalArray[i + 1])
                {
                    temp[j++] = finalArray[i];

                }
            }
            temp[j++] = finalArray[size - 1];

            for (int i = 0; i < j; i++)
            {
                finalArray[i] = temp[i];
            }
            return j;
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            int length = 0;

            Console.WriteLine("Enter Length of Array");
            length = Convert.ToInt32(Console.ReadLine());
            int[] mainArray = new int[length];

            //Accepting Array of user

            for (int i = 0; i < length; i++)
            {
                mainArray[i] = Convert.ToInt32(Console.ReadLine());
            }

            RemoveDuplicateElement obj = new RemoveDuplicateElement();

            int resultLength = obj.DeleteElement(mainArray, length);

            Console.WriteLine("Modeified Array");
            for (int i = 0; i < resultLength; i++)
            {
                Console.WriteLine("Modified Element " + mainArray[i]);
            }
        }
    }
}
