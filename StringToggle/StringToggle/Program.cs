﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringToggle
{
    public class StringToggle
    {
        public string CaseToggle(string fname)
        {
            char[] arr = fname.ToCharArray();

            for (int i = 0; i < arr.Length; i++)
            {
                if ((arr[i] >= 'A') && (arr[i] <= 'Z'))
                {
                    arr[i] = (char)(arr[i] + 32);
                }
                else if ((arr[i] >= 'a') && (arr[i] <= 'z'))
                {
                    arr[i] = (char)(arr[i] - 32);
                }


            }
            return new string(arr);
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            string name;
            name = Console.ReadLine();
            StringToggle obj = new StringToggle();

            string result = obj.CaseToggle(name);

            Console.WriteLine(result);
        }
    }
}
