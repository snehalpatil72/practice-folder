﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondMaximum
{
    public class Maximum
    {
        public int FindMaximum(int[] arrrayElement)
        {
            int maximumNumber = arrrayElement[0];
            int secondMax = arrrayElement[0];

            for (int i = 0; i < arrrayElement.Length; i++)
            {
                if (arrrayElement[i] > maximumNumber)
                {
                    secondMax = maximumNumber;
                    maximumNumber = arrrayElement[i];

                }

            }
            return secondMax;
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            int size = 0;
            Console.WriteLine("Enter Size of Array");
            size = Convert.ToInt32(Console.ReadLine());
            int[] ArrayValue = new int[size];

            Console.WriteLine("Enter Array Element");
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine("Enter " + i + " Element");
                ArrayValue[i] = Convert.ToInt32(Console.ReadLine());
            }

            Maximum mboj = new Maximum();
            int result = mboj.FindMaximum(ArrayValue);

            Console.WriteLine("second maximum " + result);

        }
    }
}
