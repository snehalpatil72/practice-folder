﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReverseString
{
    public class StringFunction
    {
        public string StringReverse(string stringReturn)
        {
            int length = stringReturn.Length - 1;
            string resultString = "";

            while (length >= 0)
            {
                resultString = resultString + stringReturn[length];
                length--;
            }
            return resultString;
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            string data;

            Console.WriteLine("Enter the String");
            data = Console.ReadLine();

            StringFunction obj = new StringFunction();

            string result = obj.StringReverse(data);

            Console.WriteLine(result);

        }
    }
}
