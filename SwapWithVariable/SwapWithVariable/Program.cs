﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwapWithVariable
{
    public class SwapNumber
    {
        public void SwapNumberMethod(int numberOne, int numberSecond)
        {
            int temp = 0;

            temp = numberOne;
            numberOne = numberSecond;
            numberSecond = temp;

            Console.WriteLine("Number After Swapping");

            Console.WriteLine(numberOne + " " + numberSecond);
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            int firstNumber = 0;
            int secondNumber = 0;

            Console.WriteLine("Enter First Number");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Second Number");
            secondNumber = Convert.ToInt32(Console.ReadLine());

            SwapNumber obj = new SwapNumber();

            obj.SwapNumberMethod(firstNumber, secondNumber);

        }
    }
}
